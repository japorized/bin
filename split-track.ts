#!/usr/bin/env -S deno run --allow-run --allow-read

import {
  Command,
  parseArgs,
} from "https://gitlab.com/japorized/rawr/-/raw/v0.1.0/mod.ts";

const songInfoPattern = /^#(\d+)\. (.+) - (.*)$/g;
const cmd: Command = {
  name: "split-track",
  description:
    "Split a track into smaller tracks. Useful for splitting up a long track that's comprised of multiple tracks.",
  takesArguments: false,
  flags: [
    {
      name: "songList",
      description: `Path to song list (line pattern: ${songInfoPattern} )`,
      valueType: "string",
      short: "l",
    },
    {
      name: "track",
      description: "Path to track to be split",
      valueType: "string",
      short: "t",
    },
    {
      name: "discNum",
      description: "Disc number (optional)",
      valueType: "string",
      short: "d",
    },
  ],
};

const cmdArgs = parseArgs<
  { songList: string; track: string; discNum?: string }
>(cmd);

if (!cmdArgs.flags?.songList) {
  console.error("Please provide song list file.");
  Deno.exit(1);
}

if (!cmdArgs.flags.track) {
  console.error("Please provide track path.");
  Deno.exit(1);
}

// change directory into the current directory where the script is ran
Deno.chdir(Deno.cwd());

const songlistPath = Deno.realPathSync(cmdArgs.flags.songList);
const trackPath = Deno.realPathSync(cmdArgs.flags.track);
const diskNumber = cmdArgs.flags?.discNum;

const trackExt = trackPath.slice(trackPath.lastIndexOf("."));

const formatTrackTime = (trackTimeStr: string) => {
  let [h, m, s] = trackTimeStr.split(":");
  if (!s) {
    s = m;
    m = h;
    h = "00";
  }

  return `${h.padStart(2, "0")}:${m.padStart(2, "0")}:${s.padStart(2, "0")}`;
};

const setFilename = (trackNum: string, trackName: string) => {
  if (!diskNumber) {
    return `${trackNum}. ${trackName}${trackExt}`;
  } else {
    return `${diskNumber}-${trackNum}. ${trackName}${trackExt}`;
  }
};

const textDecoder = new TextDecoder("utf-8");
const rawData = Deno.readFileSync(songlistPath);

const data = textDecoder.decode(rawData);

type SongInfo = {
  songFilename: string;
  songStart: string;
  songEnd: string | null;
};

const songInfoCollection: SongInfo[] = data.split("\n")
  .filter(Boolean)
  .map((s) => {
    const [match] = s.matchAll(songInfoPattern);
    if (!match) {
      console.error(`Error while parsing "${s}". Exiting`);
      console.log(match);
      Deno.exit(1);
    } else {
      return {
        songFilename: setFilename(match[1], match[2]),
        songStart: formatTrackTime(match[3]),
        songEnd: null,
      };
    }
  });
for (const [i, songInfo] of songInfoCollection.entries()) {
  const nextSong = songInfoCollection[i + 1];
  if (nextSong) {
    songInfo.songEnd = nextSong.songStart;
  }
}

// console.log(songInfoCollection);

const sliceForTrack = (outputFile: string, start: string, end: string | null) =>
  Deno.run({
    cmd: [
      "ffmpeg",
      "-loglevel",
      "quiet",
      "-y", // overwrite existing files
      "-i",
      trackPath,
      "-ss",
      start,
      ...(end ? ["-to", end] : []),
      "-c",
      "copy",
      outputFile,
    ],
  });

const converts = [];
for (const songInfo of songInfoCollection) {
  const p = sliceForTrack(
    songInfo.songFilename,
    songInfo.songStart,
    songInfo.songEnd,
  );
  converts.push(p.status());
}

const conversionSettlements = await Promise.allSettled(converts);
const failures = conversionSettlements.filter((settlements) =>
  settlements.status === "rejected"
);

if (failures.length) {
  console.log("Failed conversions were found");
  console.log(failures);
  console.log("Please check for yourselves");
} else {
  console.log("All green desu!");
}
