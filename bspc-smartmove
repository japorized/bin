#!/bin/bash
# Set window to floating mode, then snap it to top right corner

resolution="$(getRes)"
monitor_width="$(echo "$resolution" | cut -d ' ' -f1)"
monitor_height="$(echo "$resolution" | cut -d ' ' -f2)"
monitor_x_offset="$(echo "$resolution" | cut -d ' ' -f3)"
monitor_y_offset="$(echo "$resolution" | cut -d ' ' -f4)"

is_focused_window_floating() {
  bspc query -N -n focused.floating > /dev/null
  if [ $? -eq 0 ]; then
    echo true
  else
    echo false
  fi
}

has_node_in_dir_in_same_monitor() {
  bspc query -N -n "$1" -m focused
}

current_monitor="$(bspc query -M -m focused --names)"

case "$1" in
  up)
    if ($(is_focused_window_floating)); then
      bspc node -v 0 -20
    elif [ -n "$(has_node_in_dir_in_same_monitor north)" ]; then
      bspc node -s north
    else
      bspc node -s north --follow -m north --follow
    fi
    ;;
  down)
    if ($(is_focused_window_floating)); then
      bspc node -v 0 20
    elif [ -n "$(has_node_in_dir_in_same_monitor south)" ]; then
      bspc node -s south
    else
      bspc node -s south --follow -m south --follow
    fi
    ;;
  left)
    if ($(is_focused_window_floating)); then
      bspc node -v -20 0
    elif [ -n "$(has_node_in_dir_in_same_monitor west)" ]; then
      bspc node -s west
    else
      bspc node -s west --follow -m west --follow
    fi
    ;;
  right)
    if ($(is_focused_window_floating)); then
      bspc node -v 20 0
    elif [ -n "$(has_node_in_dir_in_same_monitor east)" ]; then
      bspc node -s east
    else
      bspc node -s east --follow -m east --follow
    fi
    ;;

  # These should only be used by floating windows
  top-left)
    if ($(is_focused_window_floating)); then
      window_xpos=$(returnWindowInfo.sh x)
      window_ypos=$(returnWindowInfo.sh y)
      bspc node -v "$(( monitor_x_offset - window_xpos ))" "$(( monitor_y_offset - window_ypos ))"
    fi
    ;;
  top-right)
    if ($(is_focused_window_floating)); then
      window_width=$(returnWindowInfo.sh w)
      window_xpos=$(returnWindowInfo.sh x)
      window_ypos=$(returnWindowInfo.sh y)
      bspc node -v "$(( monitor_x_offset + monitor_width - window_width - window_xpos ))" "$(( monitor_y_offset - window_ypos ))"
    fi
    ;;
  bottom-left)
    if ($(is_focused_window_floating)); then
      window_height=$(returnWindowInfo.sh h)
      window_xpos=$(returnWindowInfo.sh x)
      window_ypos=$(returnWindowInfo.sh y)
      bspc node -v "$(( monitor_x_offset - window_xpos ))" "$(( monitor_y_offset + monitor_height - window_height - window_ypos ))"
    fi
    ;;
  bottom-right)
    if ($(is_focused_window_floating)); then
      window_width=$(returnWindowInfo.sh w)
      window_height=$(returnWindowInfo.sh h)
      window_xpos=$(returnWindowInfo.sh x)
      window_ypos=$(returnWindowInfo.sh y)
      bspc node -v "$(( monitor_x_offset + monitor_width - window_width - window_xpos ))" "$(( monitor_y_offset + monitor_height - window_height - window_ypos ))"

    fi
    ;;
  top-center)
    if ($(is_focused_window_floating)); then
      window_width=$(returnWindowInfo.sh w)
      window_xpos=$(returnWindowInfo.sh x)
      window_ypos=$(returnWindowInfo.sh y)
      offset_center_x="$(( monitor_width / 2 - window_width / 2 ))"
      diff_x="$(( monitor_x_offset + offset_center_x - window_xpos ))"
      bspc node -v "$diff_x" "$(( monitor_y_offset - window_ypos ))"
    fi
    ;;
  bottom-center)
    if ($(is_focused_window_floating)); then
      window_width=$(returnWindowInfo.sh w)
      window_height=$(returnWindowInfo.sh h)
      window_xpos=$(returnWindowInfo.sh x)
      window_ypos=$(returnWindowInfo.sh y)
      offset_center_x="$(( monitor_x_offset + (monitor_width / 2 - window_width / 2) ))"
      diff_x="$(( offset_center_x - window_xpos ))"
      bspc node -v "$diff_x" "$(( monitor_y_offset + monitor_height - window_height - window_ypos ))"
    fi
    ;;
  center-left)
    if ($(is_focused_window_floating)); then
      window_height=$(returnWindowInfo.sh h)
      window_xpos=$(returnWindowInfo.sh x)
      window_ypos=$(returnWindowInfo.sh y)
      offset_center_y="$(( monitor_y_offset + (monitor_height / 2 - window_height / 2) ))"
      diff_y="$(( offset_center_y - window_ypos ))"
      bspc node -v "$(( monitor_x_offset - window_xpos ))" "$diff_y"
    fi
    ;;
  center)
    if ($(is_focused_window_floating)); then
      window_width=$(returnWindowInfo.sh w)
      window_height=$(returnWindowInfo.sh h)
      window_xpos=$(returnWindowInfo.sh x)
      window_ypos=$(returnWindowInfo.sh y)
      offset_center_x="$(( monitor_x_offset + (monitor_width / 2 - window_width / 2) ))"
      diff_x="$(( offset_center_x - window_xpos ))"
      offset_center_y="$(( monitor_y_offset + (monitor_height / 2 - window_height / 2) ))"
      diff_y="$(( offset_center_y - window_ypos ))"
      bspc node -v "$diff_x" "$diff_y"
    fi
    ;;
  center-right)
    if ($(is_focused_window_floating)); then
      window_width=$(returnWindowInfo.sh w)
      window_height=$(returnWindowInfo.sh h)
      window_xpos=$(returnWindowInfo.sh x)
      window_ypos=$(returnWindowInfo.sh y)
      offset_center_y="$(( monitor_y_offset + (monitor_height / 2 - window_height / 2) ))"
      diff_y="$(( offset_center_y - window_ypos ))"
      bspc node -v "$(( monitor_x_offset + monitor_width - window_width - window_xpos ))" "$diff_y"
    fi
    ;;
esac
