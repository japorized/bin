#!/usr/bin/env dash

$HOME/.config/bspwm/bspwm-colors.sh
systemctl --user restart dunst.service
start-polybar &
