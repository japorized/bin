#!/usr/bin/env bash
# sysinfo script

set -Eeuo pipefail

bold="\x1B[1m"
red="\e[31m"
grn="\e[32m"
ylw="\e[33m"
cyn="\e[36m"
blu="\e[34m"
prp="\e[35m"
dprp="\e[35;1m"
frst="\x1B[22m"
rst="\e[0m"

editor() {
  echo $EDITOR | rev | cut -d '/' -f1 | rev
}

total_packages() {
  pacman -Q | wc -l
}

tp() {
  total_packages
}

has_cmd() {
  if command -v $1 > /dev/null; then
    return 0
  else
    return 1
  fi
}

fatal() {
  echo "[fatal] $*"
  exit 1
}

get_wm_name() {
  wmctrl -m | grep Name | cut -d ' ' -f2-
}

get_os_name() {
  lsb_release -a | grep Description | sed -E 's/^.+:\s*//'
}

get_shell_cmd() {
  echo $SHELL | rev | cut -d '/' -f1 | rev
}

print_color_row() {
  echo -e "   ${red}█████   ${grn}█████   ${ylw}█████   ${cyn}█████   ${blu}█████   ${prp}█████   ${dprp}█████${rst}"
}

repeat() {
  local times="$1"
  shift 1
  for ((i=0;i<$times;i++)); do
    $@
  done
}

has_cmd wmctrl || fatal "wmctrl required"

echo -e
repeat 4 print_color_row
echo -e

echo -e "${bold}${cyn}WM${rst}${frst}#$(get_wm_name)#${bold}${cyn}OS${rst}${frst}#$(get_os_name)
${bold}${cyn}machine${rst}${frst}#$(hostname)#${bold}${cyn}shell${rst}${frst}#$(get_shell_cmd)
${bold}${cyn}editor${rst}${frst}#$(editor)#${bold}${cyn}term${rst}${frst}#$TERM
####
${bold}${cyn}sys fonts${rst}${frst}###
${bold}${dprp}en${rst}${frst}###
${prp}serif${rst}#Junicode#${prp}sans${rst}#Nimbus Sans
${prp}monospace${rst}#Hack Nerd Font### 
${bold}${dprp}ja${rst}${frst}###
${prp}serif${rst}#装甲明朝#${prp}sans${rst}#M+ 1mn
${prp}monospace${rst}#M+ 1mn### 
${bold}${dprp}zh${rst}${frst}###
${prp}serif${rst}#汉王楷Bold-Gb5#${prp}sans${rst}#汉王楷Bold-Gb5
${prp}monospace${rst}#文泉驿 真黑 Mono### 
" \
  | column -t --output-width 65 --separator '#' \
  | sed 's/^/   /'

echo -e
repeat 2 print_color_row

echo -e
echo -e "           \"Exactly, Watson. Pathetic and futile."
echo -e "          But is not all life pathetic and futile?"
echo -e "         Is not his story a microcosm of the whole?"
echo -e "                     We reach. We grasp."
echo -e "          And what is left in our hands at the end?"
echo -e "                         A shadow."
echo -e "              Or worse than a shadow — misery.\""
echo -e "                    ${grn}― Arthur Conan Doyle${rst}"
echo -e "          ${grn}Sherlock Holmes: The Ultimate Collection${rst}"
echo -e

print_color_row
